using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly PaymentContext _context;

        public SaleController(PaymentContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult GetSale(int saleId)
        {
            var sale = _context.Sales.Find(saleId);
            if(sale == null)
                return NotFound("Venda não localizada.");

            return Ok(sale);
        } 
        
        [HttpPost]
        public IActionResult CreateSale(int sellerId, List<int> productIds)
        {
            if(productIds.Count < 1)
                return BadRequest("Nenhum produto inserido.");

            Sale sale = new Sale(sellerId);
            _context.Sales.Add(sale);
            _context.SaveChanges();

            foreach(int i in productIds)
            {
                SaleProduct soldProduct = new SaleProduct { SaleId = sale.Id, ProductId = i };
                _context.SaleProducts.Add(soldProduct);
            }
            _context.SaveChanges();
            
            return Ok(sale);
        }

        [HttpPut]
        public IActionResult UpdateSaleStatus(int saleId, EnumSaleStatus newStatus)
        {
            var sale = _context.Sales.Find(saleId);
            if(sale == null)
                return NotFound("Venda não localizada.");

            if(sale.ValidateNewStatus(sale.SaleStatus, newStatus) == false)
                return BadRequest("Status inválido para essa venda.");

            sale.SaleStatus = newStatus;
            sale.UpdatedDate = DateTime.Now;

            _context.Sales.Update(sale);
            _context.SaveChanges();

            return Ok(sale);
        }


    }
}