using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public int SellerId { get; set; }
        public DateTime SaleDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public EnumSaleStatus SaleStatus { get; set; }

        public Sale(int sellerId)
        {
            SellerId = sellerId;
            SaleDate = DateTime.Now;
            SaleStatus = 0;
            UpdatedDate = DateTime.Now;
        }

        public bool ValidateNewStatus(EnumSaleStatus currentStatus, EnumSaleStatus newStatus)
        {
            if(currentStatus == (EnumSaleStatus) 0 && (newStatus == (EnumSaleStatus) 1 || newStatus == (EnumSaleStatus) 4))
                return true;
            
            if(currentStatus == (EnumSaleStatus) 1 && (newStatus == (EnumSaleStatus) 2 || newStatus == (EnumSaleStatus) 4))
                return true;

            if(currentStatus == (EnumSaleStatus) 2 && newStatus == (EnumSaleStatus) 3)
                return true;

            return false;
        }
    }
}