using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public class SaleProduct
    {
        [Key]
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int SaleId { get; set; }
    }
}