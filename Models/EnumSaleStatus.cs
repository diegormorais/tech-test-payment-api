namespace tech_test_payment_api.Models
{
    public enum EnumSaleStatus
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}